package com.kotlin.mybook.storage.entities

import androidx.annotation.NonNull
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName

@Entity
data class Book(
    @PrimaryKey(autoGenerate = true)
    @NonNull
    val id: Int? = null,
    @SerializedName("title")
    val title: String,
    @SerializedName("description")
    val description: String,
    @SerializedName("category")
    val category: String,
    @SerializedName("author")
    val author: String,
    @SerializedName("rating")
    val rating: Double,
    @SerializedName("filename")
    val filename: String,
    @SerializedName("image_filepath")
    val imgFilePath: String,
    @SerializedName("language")
    val language: String,
    @SerializedName("publisher")
    val publisher: String
)