package com.kotlin.mybook.storage

import com.kotlin.mybook.storage.entities.Book
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Query

interface BookApi {
    companion object Factory {
        fun create(): BookApi {
            val retrofit = Retrofit.Builder()
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl("https://my-json-server.typicode.com/Mukhash/my-book-db-b/")
                .build()
            return retrofit.create(BookApi::class.java)
        }
    }
    @GET("books")
    fun getBooks(): Call<List<Book>>

    @GET("books")
    fun getBooksById(@Query("author") name: String): Call<List<Book>>

}