package com.kotlin.mybook.storage.tables

import androidx.room.*
import com.kotlin.mybook.storage.entities.Author
import io.reactivex.Completable
import io.reactivex.Observable
import io.reactivex.Single

@Dao
interface AuthorDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAuthorList(authors: List<Author>): Completable

    @Query("DELETE FROM Author")
    fun clearAuthorList(): Completable

    @Query("SELECT * FROM Author")
    fun getAuthorList(): Observable<List<Author>>

    @Query("SELECT count(*) FROM Author")
    fun getNumOfAuthors(): Int

    @Delete
    fun removeAuthor(author: Author): Completable

    @Query("SELECT * FROM Author WHERE id = :id")
    fun getAuthorById(id: Int): Single<Author>
}