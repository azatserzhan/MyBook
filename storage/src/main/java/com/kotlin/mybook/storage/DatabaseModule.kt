package com.kotlin.mybook.storage

import androidx.room.Room
import com.kotlin.mybook.common.base.InjectionModule
import org.koin.core.module.Module
import org.koin.dsl.module

object DatabaseModule : InjectionModule {

    override fun create(): Module = module {
        single { BookApi.create()}
        single { Room.databaseBuilder(get(), AppDatabase::class.java, "myDataBase").build() }
        single { get<AppDatabase>().bookDao() }
    }
}