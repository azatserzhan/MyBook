package com.kotlin.mybook.storage.tables

import androidx.room.*
import com.kotlin.mybook.storage.entities.Book
import io.reactivex.Completable
import io.reactivex.Observable

@Dao
interface BookDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertBook(book: Book): Completable

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertBookList(books: List<Book>): Completable

    @Update
    fun updateBook(book: Book): Completable

    @Update
    fun updateBookList(books: List<Book>): Completable

    @Delete
    fun removeBook(book: Book): Completable

    @Delete
    fun removeBookList(books: List<Book>): Completable

    @Query("DELETE FROM Book")
    fun clearBookList(): Completable

    @Query("SELECT * FROM Book")
    fun getBookList(): Observable<List<Book>>
    @Query("SELECT count(*) FROM Book")
    fun getNumOfBooks(): Int
}