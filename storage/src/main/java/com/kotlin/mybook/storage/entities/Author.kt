package com.kotlin.mybook.storage.entities

import androidx.annotation.NonNull
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName

@Entity
data class Author (
    @PrimaryKey(autoGenerate = true)
    @NonNull
    val id: Int? = null,
    @SerializedName("author")
    val author: String,
    @SerializedName("author_photo")
    val author_photo: String,
    @SerializedName("author_description")
    val author_description : String,
    @SerializedName("books")
    val books: List<Book>,
    @SerializedName("same_authors")
    val same_authors: String
)