package com.kotlin.mybook.promos

import com.kotlin.mybook.promos.entities.PromoGroup
import retrofit2.Call

class PromoInteractor(private val api: PromosApi) {
    fun getPromos(): Call<List<PromoGroup>> {
        return api.getPromos()
    }
}