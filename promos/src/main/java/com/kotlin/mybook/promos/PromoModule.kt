package com.kotlin.mybook.promos

import com.kotlin.mybook.common.base.InjectionModule
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.core.module.Module
import org.koin.dsl.module

object PromoModule : InjectionModule {

    override fun create(): Module = module {
        single { PromosApi.create() }
        single { PromoInteractor(get()) }
        viewModel { PromoPresenter(get()) }
    }
}