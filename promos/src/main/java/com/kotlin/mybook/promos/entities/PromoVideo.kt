package com.kotlin.mybook.promos.entities

import androidx.room.Entity
import androidx.room.PrimaryKey
import org.jetbrains.annotations.NotNull

@Entity
class PromoVideo(
    @PrimaryKey(autoGenerate = true)
    @NotNull
    val id: Int? = null,
    val name: String,
    val video: String
): PromoTypes {
    override fun getType(): Int {
        return 3
    }
}