package com.kotlin.mybook.promos

import com.example.authorinfo.AuthorApi
import com.kotlin.mybook.promos.entities.PromoGroup
import com.kotlin.mybook.storage.entities.Author
import io.reactivex.Single
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Query

interface PromosApi {
    companion object Factory {
        fun create(): PromosApi {
            val retrofit = Retrofit.Builder()
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl("https://my-json-server.typicode.com/Mukhash/my-book-db/")
                .build()
            return retrofit.create(PromosApi::class.java)
        }
    }

    @GET("promos")
    fun getPromos(): Call<List<PromoGroup>>
}