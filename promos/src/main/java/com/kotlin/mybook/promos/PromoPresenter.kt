package com.kotlin.mybook.promos

import com.kotlin.mybook.common.base.BasePresenter
import com.kotlin.mybook.promos.entities.PromoGroup
import retrofit2.Call


class PromoPresenter(private val interactor: PromoInteractor) : BasePresenter<PromoContract.View>(),
    PromoContract.Presenter {
    override fun getPromos(): Call<List<PromoGroup>> {
        return interactor.getPromos()
    }


}