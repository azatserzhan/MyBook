package com.kotlin.mybook.promos

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.kotlin.mybook.storage.entities.Book
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.promo_book_item.view.*

class PromoChildAdapter(private val clickListener: (position: Int) -> Unit) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private val list = mutableListOf<Book>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val itemView = LayoutInflater.from(parent.context)
            .inflate(R.layout.promo_book_item, parent, false)
        return ChildViewHolder(itemView)
    }

    override fun getItemCount() = list.size

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        (holder as ChildViewHolder).bind(list[position], clickListener)
    }

    fun addItem(book: Book) {
        list.add(book)
        notifyDataSetChanged()
    }


    inner class ChildViewHolder(itemView: View): RecyclerView.ViewHolder(itemView) {

        private val bookAuthor = itemView.book_author
        private val childView = itemView.child_view
        private val rating = itemView.star_rating
        private val bookImg = itemView.book_img

        fun bind(
            book: Book,
            clickListener: (position: Int) -> Unit
        ) {
            bookAuthor.text = book.author
            rating.rating = book.rating.toFloat()
            Picasso.get().load(book.imgFilePath).into(bookImg)
            childView.setOnClickListener {
                clickListener(adapterPosition)
            }
        }
    }
}