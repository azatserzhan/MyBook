package com.kotlin.mybook.promos.entities

import androidx.annotation.NonNull
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class PromoGroup (
    @PrimaryKey(autoGenerate = true)
    @NonNull
    val id: Int? = null,
    val name: String,
    val image: String,
    val promo_type: Int,
    val books: String
): PromoTypes {
    override fun getType(): Int {
        return  promo_type
    }
}