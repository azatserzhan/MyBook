package com.kotlin.mybook.promos

import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.kotlin.mybook.booklist.book_info.BookInfoFragment
import com.kotlin.mybook.promos.entities.FirstPromoItem
import com.kotlin.mybook.promos.entities.PromoGroup
import com.kotlin.mybook.promos.entities.PromoTypes
import com.kotlin.mybook.storage.BookApi
import com.kotlin.mybook.storage.entities.Book
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.promo_first_item.view.*
import kotlinx.android.synthetic.main.promo_rv_item.view.*
import kotlinx.android.synthetic.main.promos_default_item.view.*
import kotlinx.android.synthetic.main.promos_default_item.view.cardView
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class PromosAdapter(private val clickListener: (position: Int) -> Unit) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private val promoList = mutableListOf<PromoTypes>()
    private val viewPool = RecyclerView.RecycledViewPool()
    private val firstItemPool = RecyclerView.RecycledViewPool()
    private val api: BookApi = BookApi.create()
    override fun getItemViewType(position: Int): Int {
        return promoList[position].getType()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val itemView: View? = when (viewType) {
            0 -> LayoutInflater.from(parent.context)
                .inflate(R.layout.promo_first_item, parent, false)
            1 -> LayoutInflater.from(parent.context)
                .inflate(R.layout.promos_default_item, parent, false)
            2 -> LayoutInflater.from(parent.context)
                .inflate(R.layout.promo_rv_item, parent, false)
            else -> null
        }
        return when (viewType) {
            0 -> FirstItemViewHolder(
                itemView!!
            )
            1 -> PromosViewHolder(
                itemView!!
            )
            else -> PromoRvViewHolder(
                itemView!!
            )
        }
    }

    override fun getItemCount() = promoList.size

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when (getItemViewType(position)) {
            0 -> {
                (holder as FirstItemViewHolder).bind(
                    clickListener
                )
                val firstAdapter = FirstItemAdapter(clickListener = {

                })
                val firstManager =
                    LinearLayoutManager(holder.rv.context, LinearLayoutManager.HORIZONTAL, false)
                firstManager.initialPrefetchItemCount = 4
                holder.rv.setHasFixedSize(true)
                holder.rv.apply {
                    layoutManager = firstManager
                    adapter = firstAdapter
                    setRecycledViewPool(firstItemPool)
                }
                api.getBooks().enqueue(object: Callback<List<Book>> {
                    override fun onResponse(
                        call: Call<List<Book>>,
                        response: Response<List<Book>>
                    ) {
                        if (response.code() == 200) {
                            response.body()!!.let { firstAdapter.addItems(it)}
                        }
                    }
                    override fun onFailure(call: Call<List<Book>>, t: Throwable) {
                        Log.d("BooksLoad", "ERRROR")
                    }
                })
            }
            1 -> (holder as PromosViewHolder).bind(promoList[position] as PromoGroup, clickListener)
            2 -> {
                (holder as PromoRvViewHolder).bind(
                    promoList[position] as PromoGroup,
                    clickListener
                )
                val childAdapter = PromoChildAdapter(clickListener)
                val childManager =
                    LinearLayoutManager(holder.rv.context, LinearLayoutManager.HORIZONTAL, false)
                childManager.initialPrefetchItemCount = 4
                holder.rv.setHasFixedSize(true)
                //holder.rv.setItemViewCacheSize(20)
                holder.rv.apply {
                    layoutManager = childManager
                    adapter = childAdapter
                    setRecycledViewPool(viewPool)
                }
                val promoGroup: PromoGroup = promoList[position] as PromoGroup
                val string = promoGroup.books
                api.getBooks().enqueue(object: Callback<List<Book>> {
                    override fun onResponse(
                        call: Call<List<Book>>,
                        response: Response<List<Book>>
                    ) {
                        if (response.code() == 200) {
                            val books = response.body()!!
                            for (num in string.split(" ")) {
                                for (book in books) {
                                    if (book.id == num.toInt()) { childAdapter.addItem(book)}
                                }
                            }
                        }
                    }
                    override fun onFailure(call: Call<List<Book>>, t: Throwable) {
                        Log.d("BooksLoad", "ERRROR")
                    }
                })
            }
        }
    }

    private fun setUpList() {
        promoList.add(FirstPromoItem())
    }

    fun addItems(list: List<PromoGroup>) {
        promoList.clear()
        setUpList()
        promoList.addAll(list)
        notifyDataSetChanged()
    }

    inner class FirstItemViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        private val cardView = itemView.cardView
        val rv = itemView.first_item_rv
        fun bind(
            clickListener: (position: Int) -> Unit
        ) {
            cardView.setOnClickListener {
                clickListener(adapterPosition)
            }
        }
    }
    inner class PromoRvViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        private val groupName = itemView.group_rv_name
        private val cardViewRV = itemView.cardViewRV
        private val rvImage = itemView.group_rv_img
        var rv = itemView.child_recycler
        fun bind(
            promoGroup: PromoGroup,
            clickListener: (position: Int) -> Unit
        ) {
            groupName.text = promoGroup.name
            Picasso.get().load(promoGroup.image).into(rvImage)
            cardViewRV.setOnClickListener {
                clickListener(adapterPosition)
            }
        }
    }
    inner class PromosViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        private val groupName = itemView.group_name
        private val groupImg = itemView.group_img
        private val cardView = itemView.cardView

        fun bind(promoGroup: PromoGroup, clickListener: (position: Int) -> Unit) {

            groupName.text = promoGroup.name
            Picasso.get().load(promoGroup.image).resize(400, 400).into(groupImg)
            cardView.setOnClickListener {
                clickListener(adapterPosition)
            }
        }
    }
}