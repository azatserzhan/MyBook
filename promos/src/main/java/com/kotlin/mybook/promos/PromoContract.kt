package com.kotlin.mybook.promos

import com.kotlin.mybook.common.base.MvpPresenter
import com.kotlin.mybook.common.base.MvpView
import com.kotlin.mybook.promos.entities.PromoGroup
import retrofit2.Call

interface PromoContract {
    interface View : MvpView

    interface Presenter : MvpPresenter<View> {
        fun getPromos(): Call<List<PromoGroup>>
    }
}
