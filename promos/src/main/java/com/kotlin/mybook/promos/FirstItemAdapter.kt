package com.kotlin.mybook.promos

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.RecyclerView
import com.kotlin.mybook.storage.entities.Book
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.recomendation_item.view.*

class FirstItemAdapter(private val clickListener: (position: Int) -> Unit) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    private val fragmentManager = Fragment().fragmentManager
    private val list = mutableListOf<Book>()
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val itemView = LayoutInflater.from(parent.context)
            .inflate(R.layout.recomendation_item, parent, false)
        return FirstItemViewHolder(itemView)
    }

    override fun getItemCount(): Int = list.size

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        (holder as FirstItemViewHolder).bind(list[position], clickListener)
    }

    inner class FirstItemViewHolder(itemView: View): RecyclerView.ViewHolder(itemView) {

        private val bookImg = itemView.rec_book_img

        fun bind(
            book: Book,
            clickListener: (position: Int) -> Unit
        ) {
           Picasso.get().load(book.imgFilePath).into(bookImg)
            bookImg.setOnClickListener {
                clickListener(adapterPosition)
            }
        }
    }

    fun addItems(newList: List<Book>) {
        list.addAll(newList)
        notifyDataSetChanged()
    }
}