package com.kotlin.mybook.promos.entities

interface PromoTypes {

    fun getType(): Int
}