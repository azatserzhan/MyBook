package com.kotlin.mybook.promos

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import com.kotlin.mybook.common.base.BaseFragment
import com.kotlin.mybook.promos.entities.PromoGroup
import com.kotlin.mybook.storage.BookApi
import kotlinx.android.synthetic.main.promos_parent_layout.*
import org.koin.androidx.viewmodel.ext.android.viewModel
import io.reactivex.Observable.fromCallable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class PromosFragment() : BaseFragment<PromoContract.View, PromoContract.Presenter>(), PromoContract.View {

    private var myAdapter: PromosAdapter? = null
    private val presenterImpl: PromoPresenter by viewModel()
    override val presenter: PromoContract.Presenter
        get() = presenterImpl

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?): View? {
        retainInstance = true
        return inflater.inflate(R.layout.promos_parent_layout, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        myAdapter = PromosAdapter(
            clickListener = {
            }
        )
        initRecView()
        presenter.getPromos().enqueue(object: Callback<List<PromoGroup>> {
            override fun onResponse(
                call: Call<List<PromoGroup>>,
                response: Response<List<PromoGroup>>
            ) {
                if (response.code() == 200) {
                    response.body()?.let { myAdapter!!.addItems(it) }
                }
            }
            override fun onFailure(call: Call<List<PromoGroup>>, t: Throwable) {

            }
        })
    }

    private fun initRecView() {
        parent_recycler.setHasFixedSize(true)
        parent_recycler.setItemViewCacheSize(20)
        val manager = LinearLayoutManager(context)
        parent_recycler.apply {
            layoutManager = manager
            adapter = myAdapter
        }
    }
}

