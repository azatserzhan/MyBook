package com.kotlin.mybook.reader

import com.kotlin.mybook.common.base.BasePresenter

class ReaderPresenter : BasePresenter<ReaderContract.View>(), ReaderContract.Presenter