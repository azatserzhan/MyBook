package com.kotlin.mybook.reader

import android.content.Context
import android.os.Bundle
import android.text.Html
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.kotlin.mybook.common.base.BaseFragment
import com.kotlin.mybook.storage.entities.Book
import kotlinx.android.synthetic.main.reader.*
import org.koin.androidx.viewmodel.ext.android.viewModel

class ReaderFragment(private val book: Book) :
    BaseFragment<ReaderContract.View, ReaderContract.Presenter>(), ReaderContract.View  {

    private val presenterImpl: ReaderPresenter by viewModel()
    override val presenter: ReaderContract.Presenter
        get() = presenterImpl

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?): View? {
        retainInstance = true
        return inflater.inflate(R.layout.reader, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setHtmlText(tv_reader_content)
        tv_reader_title.text = book.title
        tv_reader_author.text = book.author
    }

    private fun setHtmlText(content: TextView){
        content.text = Html.fromHtml( getHtmlDataFromAsset(context, book.filename) )
    }

    private fun getHtmlDataFromAsset(context: Context?, name: String): String? =
        context!!.assets.open(name).bufferedReader().use { it.readText() }

}