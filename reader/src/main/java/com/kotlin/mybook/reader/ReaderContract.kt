package com.kotlin.mybook.reader

import com.kotlin.mybook.common.base.MvpPresenter
import com.kotlin.mybook.common.base.MvpView

interface ReaderContract {

    interface View : MvpView

    interface Presenter : MvpPresenter<View>
}