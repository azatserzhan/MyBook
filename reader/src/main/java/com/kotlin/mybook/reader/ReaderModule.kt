package com.kotlin.mybook.reader

import com.kotlin.mybook.common.base.InjectionModule
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.core.module.Module
import org.koin.dsl.module

object ReaderModule : InjectionModule {

    override fun create(): Module = module {
        viewModel { ReaderPresenter() }
    }
}