package com.example.authorinfo

import com.kotlin.mybook.common.base.MvpPresenter
import com.kotlin.mybook.common.base.MvpView
import com.kotlin.mybook.storage.entities.Author
import com.kotlin.mybook.storage.entities.Book
import retrofit2.Call

interface AuthorInfoContract {
    interface View : MvpView

    interface Presenter : MvpPresenter<View> {
        fun getBooksById(name: String): Call<List<Book>>
        fun getAuthorById(id: Int): Call<Author>
        fun getAuthors(): Call<List<Author>>
        fun getBooks(): Call<List<Book>>

    }
}