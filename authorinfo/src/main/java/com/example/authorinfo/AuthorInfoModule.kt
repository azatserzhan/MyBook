package com.example.authorinfo

import com.kotlin.mybook.common.base.InjectionModule
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.core.module.Module
import org.koin.dsl.module

object AuthorInfoModule:InjectionModule {

    override fun create(): Module = module {
        single { AuthorInteractor(get(), get()) }
        viewModel { AuthorInfoPresenter(get()) }
    }
}