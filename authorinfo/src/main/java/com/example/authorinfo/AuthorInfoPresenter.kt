package com.example.authorinfo

import com.kotlin.mybook.common.base.BasePresenter
import com.kotlin.mybook.storage.entities.Author
import com.kotlin.mybook.storage.entities.Book
import retrofit2.Call


class AuthorInfoPresenter (private val interactor: AuthorInteractor) :
    BasePresenter<AuthorInfoContract.View>(), AuthorInfoContract.Presenter{
    override fun getBooksById(name: String): Call<List<Book>> {
        return interactor.getBooksById(name)
    }

    override fun getAuthorById(id: Int): Call<Author> {
        return interactor.getAuthorById(id)
    }

    override fun getAuthors(): Call<List<Author>> {
        return interactor.getAuthors()
    }

    override fun getBooks(): Call<List<Book>> {
        return interactor.getBooks()
    }
}