package com.example.authorinfo

import com.kotlin.mybook.storage.entities.Author
import io.reactivex.Single
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Query

interface AuthorApi {
    companion object Factory {
        fun create(): AuthorApi {
            val retrofit = Retrofit.Builder()
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl("https://my-json-server.typicode.com/Mukhash/my-book-db/")
                .build()
            return retrofit.create(AuthorApi::class.java)
        }
    }

    @GET("authors")
    fun getAuthors(): Call<List<Author>>

    @GET("authors")
    fun getAuthor(@Query ("author") name: String): Single<List<Author>>

    @GET("authors")
    fun getAuthorById(@Query ("id") id: Int): Call<Author>

}