package com.example.authorinfo

import com.kotlin.mybook.storage.BookApi
import com.kotlin.mybook.storage.entities.Author
import com.kotlin.mybook.storage.entities.Book
import retrofit2.Call

class AuthorInteractor(
    private val authorApi: AuthorApi,
    private val bookApi: BookApi
) {
    fun getAuthors(): Call<List<Author>> = authorApi.getAuthors()
    fun getBooksById(name: String): Call<List<Book>> = bookApi.getBooksById(name)
    fun getAuthorById(id: Int): Call<Author> = authorApi.getAuthorById(id)
    fun getBooks(): Call<List<Book>> = bookApi.getBooks()
}