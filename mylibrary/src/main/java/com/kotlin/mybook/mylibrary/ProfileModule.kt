package com.kotlin.mybook.mylibrary


import com.kotlin.mybook.common.base.InjectionModule
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.core.module.Module
import org.koin.dsl.module

object ProfileModule : InjectionModule {

    override fun create(): Module = module {
        single {
            ProfileAPI(get())
        }
        single { ProfileInteractor(get()) }
        viewModel { ProfilePresenter(get()) }
    }

}