package com.kotlin.mybook.mylibrary

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.kotlin.mybook.common.base.BaseFragment
import org.koin.androidx.viewmodel.ext.android.viewModel

class ProfileFragment() : BaseFragment<ProfileContract.View, ProfileContract.Presenter>(), ProfileContract.View {

    private val presenterImpl: ProfilePresenter by viewModel()
    override val presenter: ProfileContract.Presenter
        get() = presenterImpl

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?): View? {
        retainInstance = true
        return inflater.inflate(R.layout.fragment_profile, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
    }
}
