package com.kotlin.mybook.mylibrary

import com.kotlin.mybook.mylibrary.ProfileAPI

class ProfileInteractor(
    private val profileAPI: ProfileAPI/*,
    private val bookListGetterFromServer: BookListGetterFromServer*/) {

    fun getNumOfBooks():Int =
        profileAPI.getNumOfBooks()
}