package com.kotlin.mybook.mylibrary


import com.kotlin.mybook.storage.tables.*

class ProfileAPI(
    private val wereReadDao: BookDao){

    fun getNumOfBooks() =
        wereReadDao.getNumOfBooks()
}