package com.kotlin.mybook

import com.example.authorinfo.AuthorInfoModule
import com.kotlin.mybook.booklist.book_info.BookInfoModule
import com.kotlin.mybook.booklist.books.BookListModule
import com.kotlin.mybook.mylibrary.ProfileModule
import com.kotlin.mybook.booklist.categories.BookCategoriesModule
import com.kotlin.mybook.promos.PromoModule
import com.kotlin.mybook.reader.ReaderModule
import com.kotlin.mybook.storage.DatabaseModule
import com.kotlin.search.SearchListModule
import org.koin.core.module.Module

object KoinModules {
    val modules: List<Module> =
        listOf(
            DatabaseModule.create(),
            BookCategoriesModule.create(),
            BookListModule.create(),
            ReaderModule.create(),
            ProfileModule.create(),
            PromoModule.create(),
            BookInfoModule.create(),
            SearchListModule.create(),
            AuthorInfoModule.create()
        )
}