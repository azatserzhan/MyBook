package com.kotlin.mybook.greeting_activity

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.kotlin.mybook.MainActivity
import com.kotlin.mybook.R
import kotlinx.android.synthetic.main.greeting_fragment.*

class GreetingFragment: Fragment() {

    companion object {
        fun create() = GreetingFragment()
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?): View? {
        retainInstance = true
        return inflater.inflate(R.layout.greeting_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        var fragment = 0
        ibtn_next.setOnClickListener {
            when (fragment){
                0 -> {
                    firstFragment()
                    fragment++
                }
                1 -> {
                    secondFragment()
                    fragment++
                }
                2 -> {
                    thirdFragment()
                    fragment++
                }
                3 -> {
                    finalFragment()
                }
            }
        }
    }

    private fun firstFragment(){
        img_greeting.setImageResource(R.drawable.logo3)
        val str = "Большая библиотека на русском языке!"
        txt_top.text = str
        txt_bottom.visibility = View.GONE
    }

    private fun secondFragment(){
        img_greeting.setImageResource(R.drawable.logo1)
        val str = "Читайте и слушайте где угодно!"
        txt_top.text = str
    }

    private fun thirdFragment(){
        img_greeting.setImageResource(R.drawable.logo5)
        val str = "Подскажем, что выбрать!"
        txt_top.text = str
    }

    private fun finalFragment(){
        img_greeting.setImageResource(R.drawable.logo4)
        val str = "Давайте попробуем!"
        txt_top.text = str
        ibtn_next.visibility = View.GONE
        txt_start.visibility = View.VISIBLE
        txt_start.setOnClickListener {
            val intent = Intent(context, MainActivity::class.java)
            startActivity(intent)
        }
    }
}