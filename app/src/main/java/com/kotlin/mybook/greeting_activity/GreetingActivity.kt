package com.kotlin.mybook.greeting_activity

import android.os.Bundle
import com.kotlin.mybook.R
import com.kotlin.mybook.common.base.AppRouterContract
import com.kotlin.mybook.common.base.BaseActivity

class GreetingActivity: BaseActivity(), AppRouterContract {

    override fun onCreate(savedInstanceState: Bundle?) {
        Thread.sleep(2000)
        setTheme(R.style.AppTheme)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_greeting)
        showGreeting()
    }

    override fun showGreeting() {
        replaceFragment(GreetingFragment.create())
    }

    override fun onStop() {
        super.onStop()
        finish()
    }


}