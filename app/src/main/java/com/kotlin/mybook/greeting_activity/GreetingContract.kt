package com.kotlin.mybook.greeting_activity

import com.kotlin.mybook.common.base.MvpPresenter
import com.kotlin.mybook.common.base.MvpView

interface GreetingContract {
    interface View: MvpView {

    }

    interface Presenter : MvpPresenter<View> {

    }
}