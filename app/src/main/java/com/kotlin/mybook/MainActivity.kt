package com.kotlin.mybook

import android.os.Bundle
import androidx.fragment.app.FragmentManager
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.kotlin.mybook.booklist.categories.BookCategoriesFragment
import com.kotlin.mybook.common.base.BaseActivity
import com.kotlin.mybook.mylibrary.ProfileFragment
import com.kotlin.mybook.promos.PromosFragment
import com.kotlin.mybook.storage.BookApi
import com.kotlin.search.SearchListFragment

class MainActivity  : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        navbar()
    }

    private fun navbar() {
        val bottomNavigation : BottomNavigationView = findViewById(R.id.btm_nav)
        replaceFragment(BookCategoriesFragment())

        bottomNavigation.setOnNavigationItemSelectedListener { item ->
            when (item.itemId) {
                R.id.library -> replaceFragment(BookCategoriesFragment())
                R.id.choice -> replaceFragment(PromosFragment())
                R.id.search -> replaceFragment(SearchListFragment())
                R.id.profile -> replaceFragment(ProfileFragment())
            }
            true
        }
    }
}
