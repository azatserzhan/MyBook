package com.kotlin.mybook

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.kotlin.mybook.greeting_activity.GreetingActivity
import java.lang.Exception

@Deprecated("using another launcher")
class SplashScreenActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splashscreen)

        val background = object : Thread() {
            override fun run() {
                try {
                    Thread.sleep(2000)
                    val intent = Intent(baseContext, GreetingActivity::class.java)
                    startActivity(intent)
//                  val sharedPreferences = getSharedPreferences(getString(R.string.app_name), Context.MODE_PRIVATE)
//                  if (sharedPreferences.getBoolean("firstLaunch", true)) {
//                      val intent = Intent(this, GreetingActivity::class.java)
//                      startActivity(intent)
//                      val editor = sharedPreferences.edit()
//                      editor.putBoolean("firstLaunch", false)
//                      editor.commit()
//                  }
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        }
        background.start()
    }
}
