package com.kotlin.mybook.booklist.books

import android.content.Context
import com.kotlin.mybook.common.base.MvpPresenter
import com.kotlin.mybook.common.base.MvpView
import com.kotlin.mybook.storage.entities.Book
import io.reactivex.Completable
import io.reactivex.disposables.Disposable

interface BookListContract {

    interface View : MvpView{
        fun getJsonDescription(): List<Book>
    }

    interface Presenter : MvpPresenter<View> {

        fun insertBookList(bookList: List<Book>): Completable
        fun getBookList(title: String): List<Book>?
    }
}
