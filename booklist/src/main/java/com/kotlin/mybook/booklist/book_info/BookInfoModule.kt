package com.kotlin.mybook.booklist.book_info

import com.kotlin.mybook.common.base.InjectionModule
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.core.module.Module
import org.koin.dsl.module

object BookInfoModule : InjectionModule {

    override fun create(): Module = module {

        viewModel { BookInfoPresenter(get()) }
    }

}