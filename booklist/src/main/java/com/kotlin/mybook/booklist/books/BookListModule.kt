package com.kotlin.mybook.booklist.books

import com.kotlin.mybook.common.base.InjectionModule
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.core.module.Module
import org.koin.dsl.module

object BookListModule : InjectionModule {

    override fun create(): Module = module {

        viewModel { BookListPresenter(get()) }
    }

}