package com.kotlin.mybook.booklist.books

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.SearchView
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.gson.GsonBuilder
import com.google.gson.reflect.TypeToken
import com.kotlin.mybook.booklist.R
import com.kotlin.mybook.booklist.book_info.BookInfoFragment
import com.kotlin.mybook.common.base.BaseFragment
import com.kotlin.mybook.storage.entities.Book
import kotlinx.android.synthetic.main.fragment_booklist.*
import org.koin.androidx.viewmodel.ext.android.viewModel

class BookListFragment(
    private val title: String,
    private val countOfBook: String
) : BaseFragment<BookListContract.View, BookListContract.Presenter>(), BookListContract.View, SearchView.OnQueryTextListener {

    private val presenterImpl: BookListPresenter by viewModel()
    override val presenter: BookListContract.Presenter
        get() = presenterImpl

    private var bookListAdapter: BookListAdapter? = null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?): View?{
        retainInstance = true
        return inflater.inflate(R.layout.fragment_booklist, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setFragment()
        presenter.insertBookList(getJsonDescription())
        setBookInfoList()
        searchBar!!.setOnQueryTextListener(this)
    }

    private fun setFragment(){
        tv_book_list_title.text = title
        tv_count_of_book.text = countOfBook
    }

    private fun setBookInfoList(){
        bookListAdapter =
            BookListAdapter(object :
                BookListAdapter.Callback {
                override fun onClick(book: Book) {
                    replaceFragment(
                        BookInfoFragment(book)
                    )
                }
            }, context!!)
        val manager = LinearLayoutManager(context)
        rv_book_list_info.apply {
            layoutManager = manager
            adapter = bookListAdapter
        }
        addBookList(presenter.getBookList(title)!!)
    }

    override fun getJsonDescription(): List<Book>{
        val text = getJsonDataFromAsset("books.json")
        return GsonBuilder().create()
            .fromJson(text, object : TypeToken<ArrayList<Book>>() {}.type)
    }

    private fun getJsonDataFromAsset(name: String): String? =
        context!!.assets.open(name).bufferedReader().use { it.readText() }

    private fun addBookList(books: List<Book>) =
        bookListAdapter?.addBookList(books)

    private fun addBook(book: Book) =
        bookListAdapter?.addBook(book)

    private fun removeBook(book: Book) =
        bookListAdapter?.removeBook(book)

    override fun onQueryTextSubmit(query: String?): Boolean {
        return false
    }

    override fun onQueryTextChange(newText: String?): Boolean {
        if (newText != null) {
            bookListAdapter?.filter(newText)
        }
        return false
    }
}
