package com.kotlin.mybook.booklist.author_info

import com.kotlin.mybook.storage.entities.Author

interface OnLoad {
    var author: Author
}