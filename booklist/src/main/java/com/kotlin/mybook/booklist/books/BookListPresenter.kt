package com.kotlin.mybook.booklist.books

import com.kotlin.mybook.booklist.BookListInteractor
import com.kotlin.mybook.common.base.BasePresenter
import com.kotlin.mybook.storage.entities.Book
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import kotlinx.coroutines.coroutineScope

class BookListPresenter(private val bookListInteractor: BookListInteractor) :
    BasePresenter<BookListContract.View>(), BookListContract.Presenter{

    private val availableToReadOfflineBookList = mutableListOf<Book>()
    private val inReadingBookList = mutableListOf<Book>()
    private val desiredToReadBookList = mutableListOf<Book>()
    private val wereReadBookList = mutableListOf<Book>()

    override fun getBookList(title: String): List<Book>? {
        setBookList()
        lateinit var list: List<Book>
        when (title){
            "Доступны офлайн" -> list = availableToReadOfflineBookList
            "Читаю" -> list = inReadingBookList
            "Хочу прочитать" -> list = desiredToReadBookList
            "Прочитал" -> list = wereReadBookList
        }
        return list
    }

    override fun insertBookList(bookList: List<Book>) =
        bookListInteractor.insertBookList(bookList)

    private fun setBookLists() =
        bookListInteractor.getBookList()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe()

    private fun setBookList(){
        books()?.forEach {
            when(it.category){
                "Хочу прочитать" -> desiredToReadBookList.add(it)
                "Читаю" -> inReadingBookList.add(it)
                "Прочитал" -> wereReadBookList.add(it)
            }
        }
    }

    private fun books() =
        view?.getJsonDescription()

}
