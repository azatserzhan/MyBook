package com.kotlin.mybook.booklist.author_info

import android.os.Build
import android.os.Bundle
import android.text.Layout.JUSTIFICATION_MODE_INTER_WORD
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.authorinfo.AuthorInfoContract
import com.example.authorinfo.AuthorInfoPresenter
import com.kotlin.mybook.booklist.R
import com.kotlin.mybook.booklist.book_info.BookInfoFragment
import com.kotlin.mybook.common.base.BaseFragment
import com.kotlin.mybook.storage.entities.Author
import com.kotlin.mybook.storage.entities.Book
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.author_information.*
import org.koin.androidx.viewmodel.ext.android.viewModel
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class AuthorInfoFragment(private val author: Author) :
    BaseFragment<AuthorInfoContract.View, AuthorInfoContract.Presenter>(),
    AuthorInfoContract.View {

    val similarAuthors = author.same_authors
    private lateinit var booksAdapter: BooksAdapter
    private lateinit var authorsAdapter: AuthorsAdapter

    private val presenterImpl: AuthorInfoPresenter by viewModel()
    override val presenter: AuthorInfoContract.Presenter
        get() = presenterImpl

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        retainInstance = true
        return inflater.inflate(R.layout.author_information, container, false)}

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        booksAdapter =
            BooksAdapter(object :
                BooksAdapter.Callback {
                override fun onClick(book: Book) {
                    replaceFragment(BookInfoFragment(book))
                }
            }, context!!)
        authorsAdapter =
            AuthorsAdapter(object :
                AuthorsAdapter.Callback {
                override fun onClick(author: Author) {
                    replaceFragment(
                        AuthorInfoFragment(
                            author
                        )
                    )
                }
            }, context!!)
        setBooksRecyclerView()
        setAuthorsRecyclerView()
        presenter.getBooksById(author.author).enqueue(object: Callback<List<Book>> {
            override fun onResponse(
                call: Call<List<Book>>,
                response: Response<List<Book>>
            ) {
                if (response.code() == 200) {
                    response.body()?.let { booksAdapter.addBooks(it) }
                }
            }
            override fun onFailure(call: Call<List<Book>>, t: Throwable) {
            }
        })

        presenter.getAuthors().enqueue(object: Callback<List<Author>> {
            override fun onResponse(
                call: Call<List<Author>>,
                response: Response<List<Author>>
            ) {
                if (response.code() == 200) {
                    val authors = response.body()!!
                    for (num in similarAuthors) {
                        for (author in authors) {
                            if (author.id == num.toString().toInt()) { authorsAdapter.addAuthor(author)}
                        }
                    }
                }
            }
            override fun onFailure(call: Call<List<Author>>, t: Throwable) {
                Log.d("authorsLoad", "ERRROR")
            }
        })
        setAuthorImg()
        setAuthorName()
        setAuthorDesc()
    }

    private fun setBooksRecyclerView() {
        all_author_books_rv.setHasFixedSize(true)
        val manager = LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
        all_author_books_rv.apply {
            layoutManager = manager
            adapter = booksAdapter
        }
    }

    private fun setAuthorsRecyclerView() {
        similar_authors_rv.setHasFixedSize(true)
        val manager = LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
        similar_authors_rv.apply {
            layoutManager = manager
            adapter = authorsAdapter
        }
    }

    private fun getAuthorId(): Int {
        return author.id!!
    }

    private fun setAuthorName() {
        author_name.text = author.author
        Log.d("author", "${author.author_description} + ${author.author_photo}")
    }

    private fun setAuthorDesc() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            author_desc.justificationMode = JUSTIFICATION_MODE_INTER_WORD
        }
        author_desc.text = author.author_description
    }

    private fun setAuthorImg() {
        Picasso.get().load(author.author_photo).into(author_image)
    }
}