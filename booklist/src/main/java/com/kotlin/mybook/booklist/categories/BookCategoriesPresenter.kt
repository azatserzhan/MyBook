package com.kotlin.mybook.booklist.categories

import com.kotlin.mybook.booklist.BookListInteractor
import com.kotlin.mybook.common.base.BasePresenter
import com.kotlin.mybook.storage.entities.Book
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers

class BookCategoriesPresenter(private val bookListInteractor: BookListInteractor) :
    BasePresenter<BookCategoriesContract.View>(), BookCategoriesContract.Presenter{

    private var countOfAvailableToReadOffline: Int = 0
    private var countOfDesiredToRead: Int = 0
    private var countOfInReading: Int = 0
    private var countOfWereRead: Int = 0

    private val desiredToReadBookList = mutableListOf<Book>()

    override fun getCountOfAvailableToReadOffline(): Int =
        countOfAvailableToReadOffline

    override fun getCountOfDesiredToRead(): Int =
        countOfDesiredToRead

    override fun getCountOfInReading(): Int =
        countOfInReading

    override fun getCountOfWereRead(): Int =
        countOfWereRead

    override fun getBookList(): Disposable? =
        bookListInteractor.getBookList()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe{
                setCountOfBookInCategories(it)
            }

    private fun setCountOfBookInCategories(bookList: List<Book>?){
        bookList?.forEach {
            when (it.category){
                "Available to read offline" -> countOfAvailableToReadOffline++
                "Desired to read" -> {
                    countOfDesiredToRead++
                    desiredToReadBookList.add(it)
                }
                "In reading" -> countOfInReading++
                "Were read" -> countOfWereRead++
            }
        }
    }

    override fun getBooks() =
        desiredToReadBookList
}