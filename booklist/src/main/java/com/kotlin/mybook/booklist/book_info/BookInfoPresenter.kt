package com.kotlin.mybook.booklist.book_info

import com.example.authorinfo.AuthorApi
import com.kotlin.mybook.booklist.BookListInteractor
import com.kotlin.mybook.common.base.BasePresenter
import com.kotlin.mybook.storage.entities.Author
import com.kotlin.mybook.storage.entities.Book
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class BookInfoPresenter(private val bookListInteractor: BookListInteractor) :
    BasePresenter<BookInfoContract.View>(), BookInfoContract.Presenter {

    override fun addToDatabase(book: Book): Disposable =
        bookListInteractor.insertBook(book)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe()

    override fun getAuthor(name: String): Single<List<Author>> = bookListInteractor.getAuthor(name)


}
