package com.kotlin.mybook.booklist.categories

import com.kotlin.mybook.common.base.MvpPresenter
import com.kotlin.mybook.common.base.MvpView
import com.kotlin.mybook.storage.entities.Book
import io.reactivex.disposables.Disposable

interface BookCategoriesContract {

    interface View : MvpView{
        fun addBooks(books: List<Book>): Boolean?
    }

    interface Presenter : MvpPresenter<View> {

        fun getCountOfAvailableToReadOffline(): Int
        fun getCountOfDesiredToRead(): Int
        fun getCountOfInReading(): Int
        fun getCountOfWereRead(): Int
        fun getBookList(): Disposable?
        fun getBooks(): MutableList<Book>
    }
}
