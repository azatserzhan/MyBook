package com.kotlin.mybook.booklist

import com.kotlin.mybook.storage.entities.Book
import io.reactivex.Observable
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET

interface BookListGetterFromServer {
    companion object Factory{
        fun create(): BookListGetterFromServer {
            val retrofit = Retrofit.Builder()
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl("https://example.url.com/")
                .build()

            return retrofit.create(BookListGetterFromServer::class.java)
        }
    }

    @GET("example.json")
    fun get(): List<Book>
}