package com.kotlin.mybook.booklist.categories

import com.example.authorinfo.AuthorApi
import com.kotlin.mybook.booklist.BookListAPI
import com.kotlin.mybook.booklist.BookListInteractor
import com.kotlin.mybook.common.base.InjectionModule
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.core.module.Module
import org.koin.dsl.module

object BookCategoriesModule : InjectionModule {

    override fun create(): Module = module {
        single { AuthorApi.create() }
        single { BookListAPI( get() ) }
        single { BookListInteractor( get(), get() ) }
        viewModel { BookCategoriesPresenter( get() ) }
    }

}