package com.kotlin.mybook.booklist.book_info

import android.content.res.AssetManager
import android.graphics.drawable.Drawable
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import com.kotlin.mybook.booklist.author_info.AuthorInfoFragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.kotlin.mybook.booklist.R
import com.kotlin.mybook.common.base.BaseFragment
import com.kotlin.mybook.reader.ReaderFragment
import com.kotlin.mybook.storage.entities.Author
import com.kotlin.mybook.storage.entities.Book
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.fragment_about_book.*
import org.koin.androidx.viewmodel.ext.android.viewModel
import java.io.IOException
import java.io.InputStream

class BookInfoFragment(private val book: Book) :
    BaseFragment<BookInfoContract.View, BookInfoContract.Presenter>(), BookInfoContract.View {
    private lateinit var author: List<Author>
    private val presenterImpl: BookInfoPresenter by viewModel()
    override val presenter: BookInfoContract.Presenter
        get() = presenterImpl

    private var bookListAdapter: BookInfoAdapter? = null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?): View? {
        retainInstance = true
        return inflater.inflate(R.layout.fragment_about_book, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setFragment()
        btn_want_to_read.setOnClickListener {
            replaceFragment(ReaderFragment(book))
        }
         author = presenter.getAuthor(book.author)
            .subscribeOn(Schedulers.io())
            .blockingGet()

        txt_author.setOnClickListener {
            replaceFragment(
                AuthorInfoFragment(author[0])
            )
        }
    }

    private fun setFragment(){
        setImage(img_bookcover, book.imgFilePath)
        txt_bookname.text = book.title
        txt_book_description.text = book.description
        txt_author.text = book.author
        rb_book_rating.rating = book.rating.toFloat()
        setBookImageList()
    }

    private fun setBookImageList(){
        bookListAdapter =
            BookInfoAdapter(object : BookInfoAdapter.Callback {
                override fun onClick(book: Book) {
                    replaceFragment(BookInfoFragment(book))
                }
            }, context!!)
        val manager = LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
        rv_try_these_books.apply {
            layoutManager = manager
            adapter = bookListAdapter
        }
    }

    private fun setImage(image: ImageView, filename: String){
        val assetManager: AssetManager = context!!.assets
        try {
            val ims: InputStream = assetManager.open(filename)
            val d = Drawable.createFromStream(ims, null)
            image.setImageDrawable(d)
        } catch (ex: IOException) {
            Log.d("Error", "Error")
        }
    }
}
