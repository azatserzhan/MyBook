package com.kotlin.mybook.booklist.author_info

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.kotlin.mybook.booklist.R
import com.kotlin.mybook.storage.entities.Book
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.author_books.view.*

class BooksAdapter(private val callback: Callback, private val context: Context) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    interface Callback { fun onClick(book: Book) }
    private val bookList = mutableListOf<Book>()
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder =
        ItemViewHolder(
            LayoutInflater.from(parent.context)
                .inflate(R.layout.author_books, parent, false)
        )

    override fun getItemCount(): Int = bookList.size
    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        (holder as ItemViewHolder).bind(bookList[position])
    }

    inner class ItemViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        private val bookCover = itemView.book_img
        private val bookTitle = itemView.book_title
        private val bookRating = itemView.book_rating
        fun bind(book: Book) {
            bookTitle.text = book.title
            bookRating.rating = book.rating.toFloat()
            Picasso.get().load(book.imgFilePath).into(bookCover)

            itemView.book_view.setOnClickListener {
                if (adapterPosition != RecyclerView.NO_POSITION)
                    callback.onClick(bookList[adapterPosition])
            }
        }
    }

    fun addBooks(books: List<Book>) {
        bookList.addAll(books)
        notifyDataSetChanged()
    }
}