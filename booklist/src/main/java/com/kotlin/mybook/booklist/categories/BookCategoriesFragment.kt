package com.kotlin.mybook.booklist.categories

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.HorizontalScrollView
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.gson.GsonBuilder
import com.google.gson.reflect.TypeToken
import com.kotlin.mybook.booklist.books.BookListFragment
import com.kotlin.mybook.booklist.R
import com.kotlin.mybook.booklist.book_info.BookInfoFragment
import com.kotlin.mybook.common.base.BaseFragment
import com.kotlin.mybook.storage.entities.Book
import kotlinx.android.synthetic.main.fragment_book_categories.*
import org.koin.androidx.viewmodel.ext.android.viewModel

class BookCategoriesFragment : BaseFragment<BookCategoriesContract.View, BookCategoriesContract.Presenter>(),
    BookCategoriesContract.View {

    companion object {
        fun create() = BookCategoriesFragment()
    }

    private val presenterImpl: BookCategoriesPresenter by viewModel()
    override val presenter: BookCategoriesContract.Presenter
        get() = presenterImpl

    private var bookListAdapter: BookImageListAdapter? = null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?): View? {
        retainInstance = true
        return inflater.inflate(R.layout.fragment_book_categories, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        presenter.getBookList()
        setBookImageList()
        setButtons()
    }

    private fun setBookImageList(){
        bookListAdapter =
            BookImageListAdapter(object : BookImageListAdapter.Callback {
                override fun onClick(book: Book) {
                    replaceFragment(BookInfoFragment(book))
                }
            }, context!!)
        val manager = LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
        rv_book_image_list.apply {
            layoutManager = manager
            adapter = bookListAdapter
        }
        //addBooks(presenter.getBooks() as List<Book>)
        addBooks(getJsonDescription())
    }
    private fun setButtons(){
        val list = listOf(
            "Доступны офлайн",
            "Хочу прочитать",
            "Читаю",
            "Прочитал"
        )
        buttonClick(b_available_to_read_offline, list[0], getCountOfAvailableToReadOffline())
        buttonClick(b_want_to_read, list[1], getCountOfDesiredToRead())
        buttonClick(b_in_reading, list[2], getCountOfInReading())
        buttonClick(b_were_read, list[3], getCountOfWereRead())
    }
    private fun buttonClick(button: Button, title: String, count: Int){
        button.setOnClickListener {
            replaceFragment(
                BookListFragment(
                    title = title,
                    countOfBook = "$count книг"
                )
            )
        }
    }

    private fun getJsonDescription(): List<Book>{
        val text = getJsonDataFromAsset(context as Context, "books.json")
        return GsonBuilder().create()
            .fromJson(text, object : TypeToken<ArrayList<Book>>() {}.type)
    }

    private fun getJsonDataFromAsset(context: Context, name: String): String? =
        context.assets.open(name).bufferedReader().use { it.readText() }

    private fun getCountOfAvailableToReadOffline() =
        presenter.getCountOfAvailableToReadOffline()

    private fun getCountOfDesiredToRead() =
        presenter.getCountOfDesiredToRead()

    private fun getCountOfInReading() =
        presenter.getCountOfInReading()

    private fun getCountOfWereRead() =
        presenter.getCountOfWereRead()

    override fun addBooks(books: List<Book>) =
        bookListAdapter?.addBooks(books)
}