package com.kotlin.mybook.booklist.author_info

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.kotlin.mybook.booklist.R
import com.kotlin.mybook.storage.entities.Author
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.same_authors.view.*


class AuthorsAdapter(private val callback: Callback, private val context: Context) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    interface Callback {
        fun onClick(author: Author)
    }

    private val authorList = mutableListOf<Author>()
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder =
        ItemViewHolder(
            LayoutInflater.from(parent.context)
                .inflate(R.layout.same_authors, parent, false)
        )

    override fun getItemCount(): Int = authorList.size
    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        (holder as ItemViewHolder).bind(authorList[position])
    }

    inner class ItemViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        private val authorImg = itemView.author_img
        private val authorName = itemView.author_small_name
        fun bind(author: Author) {
            authorName.text = author.author
            Picasso.get().load(author.author_photo).into(authorImg)
            itemView.author_view.setOnClickListener {
                if (adapterPosition != RecyclerView.NO_POSITION)
                    callback.onClick(authorList[adapterPosition])
            }
        }
    }

    fun addAuthors(authors: List<Author>) {
        authorList.addAll(authors)
        notifyDataSetChanged()
    }

    fun addAuthor(author: Author) {
        authorList.add(author)
        notifyDataSetChanged()
    }
}