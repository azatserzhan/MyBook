package com.kotlin.mybook.booklist.categories

import android.content.Context
import android.content.res.AssetManager
import android.graphics.drawable.Drawable
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.kotlin.mybook.booklist.R
import com.kotlin.mybook.storage.entities.Book
import kotlinx.android.synthetic.main.book_image.view.*
import java.io.IOException
import java.io.InputStream

class BookImageListAdapter(val callback: Callback, private val context: Context):
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private val bookList = mutableListOf<Book>()
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder =
        ItemViewHolder(
            LayoutInflater.from(parent.context)
                .inflate(R.layout.book_image, parent, false)
        )

    override fun getItemCount(): Int = bookList.size
    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        (holder as ItemViewHolder).bind(bookList[position])
    }

    inner class ItemViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val img = itemView.iv_book_cover
        fun bind(book: Book) {
            setImage(img, book.imgFilePath)
            itemView.iv_book_cover.setOnClickListener {
                if (adapterPosition != RecyclerView.NO_POSITION)
                    callback.onClick(bookList[adapterPosition])
            }
        }
    }

    interface Callback {
        fun onClick(book: Book)
    }

    fun addBooks(books: List<Book>) =
        bookList.addAll(books)

    private fun setImage(image: ImageView, filename: String){
        val assetManager: AssetManager = context.assets
        try {
            val ims: InputStream = assetManager.open(filename)
            val d = Drawable.createFromStream(ims, null)
            image.setImageDrawable(d)
        } catch (ex: IOException) {
            Log.d("Error", "Error")
        }
    }
}