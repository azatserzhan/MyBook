package com.kotlin.mybook.booklist.book_info

import com.kotlin.mybook.common.base.MvpPresenter
import com.kotlin.mybook.common.base.MvpView
import com.kotlin.mybook.storage.entities.Author
import com.kotlin.mybook.storage.entities.Book
import io.reactivex.Completable
import io.reactivex.Single
import io.reactivex.disposables.Disposable
import retrofit2.Call

interface BookInfoContract {

    interface View : MvpView

    interface Presenter : MvpPresenter<View> {
        fun addToDatabase(book: Book): Disposable
        fun getAuthor(name: String): Single<List<Author>>
    }
}
