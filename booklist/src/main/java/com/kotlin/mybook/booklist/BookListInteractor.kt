package com.kotlin.mybook.booklist

import com.example.authorinfo.AuthorApi
import com.kotlin.mybook.storage.entities.Author
import com.kotlin.mybook.storage.entities.Book
import io.reactivex.Single
import retrofit2.Call

class BookListInteractor(
    private val api: AuthorApi,
    private val bookListAPI: BookListAPI/*,
    private val bookListGetterFromServer: BookListGetterFromServer*/) {

    fun insertBook(book: Book) =
        bookListAPI.insertBook(book)

    fun insertBookList(books: List<Book>) =
        bookListAPI.insertBookList(books)

    fun updateBook(book: Book) =
        bookListAPI.updateBook(book)

    fun updateBookList(books: List<Book>) =
        bookListAPI.updateBookList(books)

    fun removeBook(book: Book) =
        bookListAPI.removeBook(book)

    fun removeBookList(books: List<Book>) =
        bookListAPI.removeBookList(books)

    fun clearBookList() =
        bookListAPI.clearBookList()

    fun getBookList() =
        bookListAPI.getBookList()

    fun getAuthor(name: String): Single<List<Author>> = api.getAuthor(name)
}