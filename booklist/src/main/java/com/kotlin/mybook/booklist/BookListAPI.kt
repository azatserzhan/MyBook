package com.kotlin.mybook.booklist

import com.kotlin.mybook.storage.entities.Book
import com.kotlin.mybook.storage.tables.*

class BookListAPI(private val bookDao: BookDao){

    fun insertBook(book: Book) =
        bookDao.insertBook(book)

    fun insertBookList(books: List<Book>) =
        bookDao.insertBookList(books)

    fun updateBook(book: Book) =
        bookDao.updateBook(book)

    fun updateBookList(books: List<Book>) =
        bookDao.updateBookList(books)

    fun removeBook(book: Book) =
        bookDao.removeBook(book)

    fun removeBookList(books: List<Book>) =
        bookDao.removeBookList(books)

    fun clearBookList() =
        bookDao.clearBookList()

    fun getBookList() =
        bookDao.getBookList()
}