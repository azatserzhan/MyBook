package com.kotlin.search

import com.kotlin.mybook.storage.entities.Book

class SearchListInteractor(
    private val bookListAPI: SearchListAPI/*,
    private val bookListGetterFromServer: BookListGetterFromServer*/) {

    fun insertBook(book: Book) =
        bookListAPI.insertBook(book)

    fun insertBookList(books: List<Book>) =
        bookListAPI.insertBookList(books)

    fun updateBook(book: Book) =
        bookListAPI.updateBook(book)

    fun updateBookList(books: List<Book>) =
        bookListAPI.updateBookList(books)

    fun removeBook(book: Book) =
        bookListAPI.removeBook(book)

    fun removeBookList(books: List<Book>) =
        bookListAPI.removeBookList(books)

    fun clearBookList() =
        bookListAPI.clearBookList()

    fun getBookList() =
        bookListAPI.getBookList()
}