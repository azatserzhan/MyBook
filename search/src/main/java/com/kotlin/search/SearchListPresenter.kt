package com.kotlin.search

import com.kotlin.mybook.booklist.BookListInteractor
import com.kotlin.mybook.common.base.BasePresenter
import com.kotlin.mybook.storage.entities.Book
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class SearchListPresenter(private val bookListInteractor: SearchListInteractor) :
    BasePresenter<SearchListContract.View>(), SearchListContract.Presenter{

    private val allBookList: MutableList<Book>? = null

    override fun getBookList(title: String): List<Book>?{
        setBookLists()
        var list: List<Book>? = null
        list = allBookList as List<Book>
        return list
    }

    private fun setBookLists() =
        bookListInteractor.getBookList()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe {
                setBookList(it)
            }

    private fun setBookList(bookList: List<Book>?){
        bookList?.forEach {
            allBookList?.add(it)
        }
    }

}
