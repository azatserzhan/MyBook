package com.kotlin.search

import com.kotlin.mybook.booklist.BookListAPI
import com.kotlin.mybook.booklist.BookListInteractor
import com.kotlin.mybook.booklist.categories.BookCategoriesPresenter
import com.kotlin.mybook.common.base.InjectionModule
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.core.module.Module
import org.koin.dsl.module

object SearchListModule : InjectionModule {

    override fun create(): Module = module {
        single { SearchListAPI( get() ) }
        single { SearchListInteractor( get() ) }
        viewModel { SearchListPresenter(get()) }
    }

}