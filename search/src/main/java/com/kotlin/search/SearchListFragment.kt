package com.kotlin.search

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.SearchView
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.gson.GsonBuilder
import com.google.gson.reflect.TypeToken
import com.kotlin.mybook.booklist.R
import com.kotlin.mybook.common.base.BaseFragment
import com.kotlin.mybook.reader.ReaderFragment
import com.kotlin.mybook.storage.entities.Book
import kotlinx.android.synthetic.main.fragment_booklist.*
import org.koin.androidx.viewmodel.ext.android.viewModel

class SearchListFragment : BaseFragment<SearchListContract.View, SearchListContract.Presenter>(), SearchListContract.View, SearchView.OnQueryTextListener{

    private val presenterImpl: SearchListPresenter by viewModel()
    override val presenter: SearchListPresenter
        get() = presenterImpl

    private var searchListAdapter: SearchListAdapter? = null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?): View? {
        retainInstance = true
        return inflater.inflate(R.layout.fragment_booklist, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setFragment()
        setBookInfoList()
        searchBar!!.setOnQueryTextListener(this)
    }
    private fun setFragment(){
        tv_book_list_title.text = "Поиск"
    }
    private fun setBookInfoList(){
        searchListAdapter =
            SearchListAdapter(object :
                SearchListAdapter.Callback {
                override fun onClick(book: Book) {
                    replaceFragment(
                        ReaderFragment(
                            book
                        )
                    )
                }
            }, context!!)
        val manager = LinearLayoutManager(context)
        rv_book_list_info.apply {
            layoutManager = manager
            adapter = searchListAdapter
        }
        addBookList(getJsonDescription())
        //addBookList(presenter.getBookList(title) as List<Book>)
    }

    private fun getJsonDescription(): List<Book>{
        val text = getJsonDataFromAsset(context as Context, "books.json")
        return GsonBuilder().create()
            .fromJson(text, object : TypeToken<ArrayList<Book>>() {}.type)
    }

    private fun getJsonDataFromAsset(context: Context, name: String): String? =
        context.assets.open(name).bufferedReader().use { it.readText() }

    private fun addBookList(books: List<Book>) {
        books.forEach {
            addBook(it)
        }
    }

    private fun addBook(book: Book) =
        searchListAdapter?.addBook(book)

    private fun removeBook(book: Book) =
        searchListAdapter?.removeBook(book)

    override fun onQueryTextSubmit(query: String?): Boolean {
        return false
    }

    override fun onQueryTextChange(newText: String?): Boolean {
        if (newText != null) {
            searchListAdapter?.filter(newText)
        }
        return false
    }
}
