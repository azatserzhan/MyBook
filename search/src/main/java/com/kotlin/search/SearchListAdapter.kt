package com.kotlin.search

import android.content.Context
import android.content.res.AssetManager
import android.graphics.drawable.Drawable
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.recyclerview.widget.RecyclerView
import com.kotlin.mybook.booklist.R
import com.kotlin.mybook.storage.entities.Book
import kotlinx.android.synthetic.main.booklist_element.view.*
import java.io.IOException
import java.io.InputStream
import java.util.*

class SearchListAdapter(val callback: Callback, private val context: Context):
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private val bookList = mutableListOf<Book>()
    private val filterBookList = mutableListOf<Book>()
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder =
        ItemViewHolder(
            LayoutInflater.from(parent.context)
                .inflate(R.layout.booklist_element, parent, false)
        )

    override fun getItemCount(): Int = bookList.size
    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        (holder as ItemViewHolder).bind(bookList[position])
    }

    inner class ItemViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        private val title = itemView.tv_book_title
        private val author = itemView.tv_book_author
        private val rating = itemView.rb_book_rating
        private val cover = itemView.iv_booklist_element_cover

        fun bind(book: Book) {
            title.text = book.title
            author.text = book.author
            rating.rating = book.rating.toFloat()
            setImage(cover, book.imgFilePath)
            itemView.book_info.setOnClickListener {
                if (adapterPosition != RecyclerView.NO_POSITION)
                    callback.onClick(bookList[adapterPosition])
            }
        }
    }

    interface Callback { fun onClick(book: Book) }

    fun addBookList(books: List<Book>) =
        bookList.addAll(books)

    fun addBook(book: Book) =
        bookList.add(book)

    fun removeBook(book: Book) =
        bookList.remove(book)

    fun filter(charText: String) {
        var charText = charText
        charText = charText.toLowerCase(Locale.getDefault())
        //filterBookList.clear()
        for (book in bookList) {
            if(!filterBookList.contains(book)) {
                filterBookList.add(book)
            }
        }
        //filterBookList.addAll(bookList)
        bookList.clear()
        if (charText.length == 0) {
            bookList.addAll(filterBookList)
        } else {
            for (wp in filterBookList) {
                if (wp.title.toLowerCase(Locale.getDefault()).contains(charText)) {
                    bookList.add(wp)
                }
            }
        }
        notifyDataSetChanged()
    }
    private fun setImage(image: ImageView, filename: String){
        val assetManager: AssetManager = context.assets
        try {
            val ims: InputStream = assetManager.open(filename)
            val d = Drawable.createFromStream(ims, null)
            image.setImageDrawable(d)
        } catch (ex: IOException) {
            Log.d("Error", "Error")
        }
    }
}