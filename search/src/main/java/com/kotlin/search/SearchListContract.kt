package com.kotlin.search

import com.kotlin.mybook.common.base.MvpPresenter
import com.kotlin.mybook.common.base.MvpView
import com.kotlin.mybook.storage.entities.Book

interface SearchListContract {

    interface View : MvpView

    interface Presenter : MvpPresenter<View> {
        fun getBookList(title: String): List<Book>?
    }
}
